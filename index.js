const fs = require('fs');
const _ = require('lodash');

const modulo = process.argv[2] || 'teste';
const moduloS = modulo + 's';

const path = _.toLower(_.camelCase(modulo));
const crudApi = [
  { acao: 'ler_um',     arg: 'id',      api: 'api(token, `' + path + '/${id}`)',                    reducer: ``, },
  { acao: 'ler_todos',  arg: '',        api: `api(token, '${path}')`,                               reducer: ``, },
  { acao: 'ler_n',      arg: 'params',  api: `api(token, '${path}', 'post', params)`,               reducer: ``, },
  { acao: 'adicionar',  arg: 'item',    api: `api(token, '${path}/set', 'post', item)`,             reducer: ``, },
  { acao: 'editar',     arg: 'item, id',api: 'api(token, `' + path + '/${id}`, ' + `'post', item)`, reducer: ``, },
  { acao: 'deletar',    arg: 'id',      api: 'api(token, `' + path + '/delete/${id}`, ' + `'post')`,reducer: ``, }
];
const respostaCrudApi = [{ res: 'funcionou', carga: 'carga', }, { res: 'falhou', carga: 'mensagem', }];
let acoesSimples = [
  { as: 'alerta', is: null, },
  { as: 'pesquisa', is: `''`, },
  { as: 'items_por_pagina', is: 5, },
  { as: 'pesquisa_exata', is: false, },
  { as: 'pesquisa_sensitiva', is: false, },
  { as: 'pesquisa_uso_data', is: 0, },
  { as: 'data_fim', is: null, },
  { as: 'data_inicio', is: null, },
  { as: 'num_paginas', is: 0, },
];
const redutor = (acao) => {
  let redutor = '';
  let carga = _.lowerCase(moduloS);
  switch (acao) {
    case crudApi[0].acao:
      redutor += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: null, ${carga}: [action.payload], };\n`;
      break;
    case crudApi[1].acao:
      redutor += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: null, ${carga}: action.payload, };\n`;
      break;
    case crudApi[2].acao:
      redutor += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: null, ${carga}: [...state.${carga}, ...action.payload], };\n`;
      break;
    case crudApi[3].acao:
      redutor += ``;
      redutor += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: null, ${carga}: [action.payload, ...state.${carga}], };\n`;
      break;
    case crudApi[4].acao:
      redutor += `\n\t\t\tindice = state.${carga}.findIndex(item => item.id === action.payload.id);`;
      redutor += `\n\t\t\titemsAtualizados = [`;
      redutor += `\n\t\t\t\t...state.${carga}.slice(0, indice),`;
      redutor += `\n\t\t\t\taction.payload,`;
      redutor += `\n\t\t\t\t...state.${carga}.slice(indice + 1)`;
      redutor += `\n\t\t\t];`;
      redutor += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: null, ${carga}: itemsAtualizados, };\n`;
      break;
    case crudApi[5].acao:
      redutor += `\n\t\t\tindice = state.${carga}.findIndex(item => item.id === action.payload.id);`;
      redutor += `\n\t\t\titemsAtualizados = [`;
      redutor += `\n\t\t\t\t...state.${carga}.slice(0, indice),`;
      redutor += `\n\t\t\t\t...state.${carga}.slice(indice + 1)`;
      redutor += `\n\t\t\t];`;
      redutor += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: null, ${carga}: itemsAtualizados, };\n`;
      break;
    default:

  }
  return redutor;
}

// =================================================================================================

const streamActionsTypes = fs.createWriteStream(`${modulo}-actions-types.js`);

streamActionsTypes.once('open', function(fd) {
  let a = '', act, typ = '', types;

  types = acoesSimples.map(simples => {
    act = _.toUpper(`${modulo}_${simples.as}`);
    return `export const ${act} = '${act}';\n`;
  });
  a += types.join('\n');

  types = crudApi.map(crud => {
    typ = respostaCrudApi.map(resposta => {
      act = _.toUpper(`${modulo}_${crud.acao}_${resposta.res}`);
      return `export const ${act} = '${act}';\n`;
    });
    return '\n' + typ.join('\n');
  });

  a += types.join('');

  streamActionsTypes.write(a);

  streamActionsTypes.end();
});

// =================================================================================================

const importesActions = (crudApi, respostaCrudApi) => {
  let a = '', crud = '';

  a += `import api from '../uteis/api';\n`;
  a += `import { firebaseAuth, } from '../config/constantes';\n\n`;

  let simples = acoesSimples.map(acao => {
    return _.toUpper(`\t${modulo}_${acao.as},\n`);
  });

  let cruds = crudApi.map(crud => {
    crud = respostaCrudApi.map(resposta => {
      return _.toUpper(`${modulo}_${crud.acao}_${resposta.res}`);
    });
    return '\t' + crud.join(', ');
  });

  a += 'import {\n';
  a += simples.join('');
  a += cruds.join(',\n');
  a += '\n';
  a += `} from './acoesTipos';\n`;
  return a;
}

const acaoSimples = acao => {
  nomeFuncao = _.camelCase(`${modulo} ${acao}`);
  tipoAcao = _.toUpper(`${modulo}_${acao}`);

  let a = '';
  a += `\nexport const ${nomeFuncao} = carga => {\n`;
  a += `\treturn {\n`;
  a += `\t\ttype: ${tipoAcao},\n`;
  a += `\t\tpayload: carga,\n`;
  a += `\t};\n`;
  a += `}\n`;
  return a;
}

const crud = (crudApi, respostaCrudApi) => {
  let funcs;
  let sucesso;
  let falha;
  let nomeFuncao;
  let tipoAcao;


  funcs = crudApi.map(crud => {
    funcs = respostaCrudApi.map(resposta => {
      nomeFuncao = _.camelCase(`${modulo} ${crud.acao} ${resposta.res}`);
      tipoAcao = _.toUpper(`${modulo}_${crud.acao}_${resposta.res}`);

      let a = '';
      a += `\nexport const ${nomeFuncao} = (${resposta.carga}) => {\n`;
      a += `\treturn {\n`;
      a += `\t\ttype: ${tipoAcao},\n`;
      a += `\t\tpayload: ${resposta.carga},\n`;
      a += `\t};\n`;
      a += `}\n`;
      return a;
    });

    sucesso = _.camelCase(`${modulo} ${crud.acao} ${respostaCrudApi[0].res}`);
    falha = _.camelCase(`${modulo} ${crud.acao} ${respostaCrudApi[1].res}`);

    let b = '';
    nomeFuncao = _.camelCase(`${modulo} ${crud.acao}`);
    b += `\nexport const ${nomeFuncao} = (${crud.arg}) => dispatch => firebaseAuth.currentUser.getIdToken(true)\n`;
    b += `\t.then(token => ${crud.api})\n`;
    b += `\t.then(response => {\n`;
    b += `\t\tlet resp = { ok: false, message: null, };\n`;
    b += `\t\tif(response.ok) {\n`;
    b += `\t\t\tdispatch(${sucesso}(response.payload));\n`;
    b += `\t\t\tresp.ok = true;\n`;
    b += `\t\t} else {\n`;
    b += `\t\t\tdispatch(${falha}(response.payload));\n`;
    b += `\t\t\tresp.ok = false;\n`;
    b += `\t\t\tresp.message = 'erro em ${nomeFuncao}';\n`;
    b += `\t\t}\n`;
    b += `\t\treturn resp;\n`;
    b += `\t})\n`;
    b += `\t.catch(err => {\n`;
    b += `\t\tconsole.log("erro em ${nomeFuncao}:", err);\n`;
    b += `\t\treturn dispatch(${falha}(err));\n`;
    b += `\t});\n\n`;

    return funcs.join('') + b;
  });

  return funcs.join('');
}

const streamActions = fs.createWriteStream(`${modulo}-actions.js`);

streamActions.once('open', function(fd) {

  let acao;
  let nomeFuncao;
  let tipoAcao;
  let a = '';

  //=======================================================

  a += importesActions(crudApi, respostaCrudApi);

  //=======================================================

  let sim = acoesSimples.map(simples => {
    return acaoSimples(simples.as);
  });

  a += sim.join('');

  //=======================================================

  a += crud(crudApi, respostaCrudApi);

  //=======================================================

  streamActions.write(a);
  streamActions.end();
});

// =================================================================================================

const redutorSimples = acao => {
  let a = '';

  const cas = _.toUpper(`${modulo}_${acao}`);
  const fff = _.camelCase(`${acao}`);

  a += `\n\t\tcase ${cas}:`;
  a += `\n\t\t\treturn { ...state, ${fff}: action.payload, };\n`;
  return a;
}

const importesReducers = (crudApi, respostaCrudApi) => {
  let a = '', crud = '';

  let simples = acoesSimples.map(acao => {
    return _.toUpper(`\t${modulo}_${acao.as},\n`);
  });

  let cruds = crudApi.map(crud => {
    crud = respostaCrudApi.map(resposta => {
      return _.toUpper(`${modulo}_${crud.acao}_${resposta.res}`);
    });
    return '\t' + crud.join(', ');
  });

  a += 'import {\n';
  a += simples.join('');
  a += cruds.join(',\n');
  a += '\n';
  a += `} from '../acoes/acoesTipos';\n\n`;
  return a;
}

const stateReducers = () => {
  let a = '';
  let args = acoesSimples.map(acao => {
    let fff = _.camelCase(acao.as);
    return `${fff}: ${acao.is},`;
  });
  a += `\nconst INITIAL_STATE = { ${_.lowerCase(moduloS)}: [], ${args.join(' ')} };\n`;
  return a;
}

const exportDefaultIni = () => {
  let a = '';
  a += `\nexport default function (state = INITIAL_STATE, action) {`;
  a += '\n\tlet indice, itemsAtualizados;';
  a += '\n\n\tswitch (action.type) {';
  a += '\n\t\t';
  return a;
}

const exportDefaultEnd = () => {
  let a = '';

  a += '\n\t\tdefault:';
  a += '\n\t\t\treturn state;\n';

  a += '\n\t}';
  a += '\n}';
  return a;
}

const casos = () => {
  let caso, tipoAcao, a = '';

  caso = crudApi.map(crud => {
      a = '';
      let carga = _.lowerCase(moduloS);

      tipoAcao = _.toUpper(`${modulo}_${crud.acao}_${respostaCrudApi[0].res}`);
      a += `\n\t\tcase ${tipoAcao}:`;
      a += redutor(crud.acao);

      tipoAcao = _.toUpper(`${modulo}_${crud.acao}_${respostaCrudApi[1].res}`);
      a += `\n\t\tcase ${tipoAcao}:`;
      a += `\n\t\t\treturn { ...state, ${acoesSimples[0].as}: action.payload, };\n`;

      return a;
  });

  return caso.join('');
}

const reducersGets = () => {
  let a = '' , nomeFuncSingular, nomeFuncPlural;

  const funcs = acoesSimples.map(acao => {
    let fff = _.camelCase(acao.as);
    let nomeFunc = _.camelCase(`get ${moduloS} ${acao.as}`);
    return `\n\nexport const ${nomeFunc} = state => state.${_.lowerCase(moduloS)}.${fff};`;
  });
  a += funcs.join('');

  nomeFuncSingular = _.camelCase(`get ${modulo}`);
  nomeFuncPlural = _.camelCase(`get ${moduloS}`);
  let nomeModuloPlural = _.lowerCase(moduloS);

  a += `\n\nexport const ${nomeFuncPlural} = state => state.${nomeModuloPlural}.${nomeModuloPlural};`;
  a += `\n\nexport const ${nomeFuncSingular} = (state, id) => state.${nomeModuloPlural}.${nomeModuloPlural}.filter(item => item.id === id)[0];`;
  return a;
}

const streamReducers = fs.createWriteStream(`${modulo}-reducers.js`);

streamReducers.once('open', function(fd) {

  let a = '', acao;

  a += importesReducers(crudApi, respostaCrudApi);

  a += stateReducers();

  a += exportDefaultIni();

  let casosSimples = acoesSimples.map(acao => redutorSimples(acao.as));
  a += casosSimples.join('');

  a += casos();

  a += exportDefaultEnd();

  a += reducersGets();

  streamReducers.write(a);

  streamReducers.write('\n');
  streamReducers.end();
});
