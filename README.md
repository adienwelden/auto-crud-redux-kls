# Pequeno automatizador de tarefas CRUD Redux que usa API externa com autenticação Firebase Auth.

## Rodar:

`npm install`
`node <prefixo dos arquivos>`


## Exemplo:

`node evento` vai gerar 3 arquivos:

```
evento-actions-types.js - contém os tipos de ações Redux
evento-actions.js - contém as ações do Redux
evento-reducers.js - contém os redutores Redux
```
